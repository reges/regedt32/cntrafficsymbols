EX=b"""\
{
  "variants": {
    "position=y_center": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_y_center"},
    "position=y_south": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_y_south"},
    "position=y_southeast": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_y_southeast"},
    "position=y_east": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_y_east"},
    "position=y_northeast": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_y_northeast"},
    "position=y_north": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_y_north"},
    "position=y_northwest": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_y_northwest"},
    "position=y_west": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_y_west"},
    "position=y_southwest": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_y_southwest"},
    "position=z_center": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_center"},
    "position=z_upper": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_upper"},
    "position=z_uppereast": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_uppereast"},
    "position=z_east": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_east"},
    "position=z_lowereast": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_lowereast"},
    "position=z_lower": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_lower"},
    "position=z_lowerwest": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_lowerwest"},
    "position=z_west": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_west"},
    "position=z_upperwest": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_upperwest"},
    "position=x_center": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_center", "y": 90},
    "position=x_upper": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_upper", "y": 90},
    "position=x_uppersouth": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_uppereast", "y": 90},
    "position=x_south": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_east", "y": 90},
    "position=x_lowersouth": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_lowereast", "y": 90},
    "position=x_lower": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_lower", "y": 90},
    "position=x_lowernorth": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_lowerwest", "y": 90},
    "position=x_north": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_west", "y": 90},
    "position=x_uppernorth": {"model": "regedt32:block/cntrafficsymbols/%s_full1_cylindrical_rod_z_upperwest", "y": 90}
  }
}
""".replace(b"\n",b"\r\n")
COLORS=("white","orange","magenta","light_blue","yellow","lime","pink","gray",
        "light_gray","cyan","purple","blue","brown","green","red","black")
for i in COLORS:
 with open(f"{i}_full1_cylindrical_rod.json","wb")as f:
  f.write(EX%((i.encode(),)*27))
