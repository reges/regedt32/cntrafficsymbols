# CNTrafficSymbols
## Credits
### Programming
* REGEdt32
### Artwork
* REGEdt32
* Manarein
* EarthHaoduo
### Localization
#### zh_cn
* REGEdt32
#### en_us
* morrison2077
#### zh_tw
* azaz-az
### Special Thanks
* Gradle Inc.
* Free Software Foundation
* FabricMC
* Python Software Foundation
