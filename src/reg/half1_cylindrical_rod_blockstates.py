EX=b"""\
{
  "variants": {
    "position=down_center": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_down_center"},
    "position=down_south": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_down_south"},
    "position=down_southeast": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_down_southeast"},
    "position=down_east": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_down_east"},
    "position=down_northeast": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_down_northeast"},
    "position=down_north": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_down_north"},
    "position=down_northwest": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_down_northwest"},
    "position=down_west": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_down_west"},
    "position=down_southwest": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_down_southwest"},
    "position=up_center": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_up_center"},
    "position=up_south": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_up_south"},
    "position=up_southeast": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_up_southeast"},
    "position=up_east": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_up_east"},
    "position=up_northeast": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_up_northeast"},
    "position=up_north": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_up_north"},
    "position=up_northwest": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_up_northwest"},
    "position=up_west": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_up_west"},
    "position=up_southwest": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_up_southwest"},
    "position=north_center": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_center"},
    "position=north_upper": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_upper"},
    "position=north_uppereast": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_uppereast"},
    "position=north_east": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_east"},
    "position=north_lowereast": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lowereast"},
    "position=north_lower": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lower"},
    "position=north_lowerwest": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lowerwest"},
    "position=north_west": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_west"},
    "position=north_upperwest": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_upperwest"},
    "position=east_center": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_center", "y": 90},
    "position=east_upper": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_upper", "y": 90},
    "position=east_uppersouth": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_uppereast", "y": 90},
    "position=east_south": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_east", "y": 90},
    "position=east_lowersouth": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lowereast", "y": 90},
    "position=east_lower": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lower", "y": 90},
    "position=east_lowernorth": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lowerwest", "y": 90},
    "position=east_north": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_west", "y": 90},
    "position=east_uppernorth": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_upperwest", "y": 90},
    "position=south_center": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_center", "y": 180},
    "position=south_upper": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_upper", "y": 180},
    "position=south_upperwest": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_uppereast", "y": 180},
    "position=south_west": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_east", "y": 180},
    "position=south_lowerwest": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lowereast", "y": 180},
    "position=south_lower": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lower", "y": 180},
    "position=south_lowereast": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lowerwest", "y": 180},
    "position=south_east": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_west", "y": 180},
    "position=south_uppereast": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_upperwest", "y": 180},
    "position=west_center": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_center", "y": 270},
    "position=west_upper": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_upper", "y": 270},
    "position=west_uppernorth": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_uppereast", "y": 270},
    "position=west_north": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_east", "y": 270},
    "position=west_lowernorth": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lowereast", "y": 270},
    "position=west_lower": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lower", "y": 270},
    "position=west_lowersouth": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_lowerwest", "y": 270},
    "position=west_south": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_west", "y": 270},
    "position=west_uppersouth": {"model": "regedt32:block/cntrafficsymbols/%s_half1_cylindrical_rod_north_upperwest", "y": 270}
  }
}
""".replace(b"\n",b"\r\n")
COLORS=("white","orange","magenta","light_blue","yellow","lime","pink","gray",
        "light_gray","cyan","purple","blue","brown","green","red","black")
for i in COLORS:
 with open(f"{i}_half1_cylindrical_rod.json","wb")as f:
  f.write(EX%((i.encode(),)*54))
